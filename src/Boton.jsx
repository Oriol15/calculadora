

export default (props) =>{
    const numeros = [1,2,3,4,5,6,7,8,9,0];
    const botonesNumeros = numeros.map((el) => (
        <button onClick={()=> props.clicar(el)} key={el}>{el}</button>
    ));

    const botonesOperaciones = <div className="operaciones"> 
                                <button onClick={()=>props.clicar("+")}>+</button>
                                <button onClick={()=>props.clicar("-")}>-</button>
                                <button onClick={()=>props.clicar("*")}>*</button>
                                <button onClick={()=>props.clicar("/")}>/</button>
                                <button onClick={()=>props.clicar("=")}>=</button>
                                <button onClick={()=>props.clicar("C")}>C</button>
                            </div>;
    return (
        <div className="botones">
            {botonesNumeros}
            {botonesOperaciones}
        </div>
    )
}