
export default (props) => {
    let resultado = "";
    if(props.num2 !== ""){
        resultado = props.num1 + props.operacion +props.num2;
    } else if(props.operacion !== ""){
        resultado = props.num1 + props.operacion;
    } else if(props.num1 !== ""){
        resultado = props.num1;
    }
    
    if (props.resultat !== ""){
        if (props.operacion === "+"){
            resultado = props.num1+props.num2;
        } else if ( props.operacion === "-"){
            resultado = props.num1-props.num2;
        } else if (props.operacion === "*"){
            resultado = props.num1 * props.num2;
        } else {
            resultado = props.num1 / props.num2;
        }
    }

    
    return (
        <div className="display">{resultado}</div>
        )
}