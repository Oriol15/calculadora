import Boton from "./Boton";
import Display from "./Display";
import './App.css';
import React, { useState} from 'react';

function App() {

  const [operacion, setOperacion] = useState("");
  const [valor1, setValor1] = useState("");
  const [valor2, setValor2] = useState("");
  const [resultat, setResultat] = useState("");

  const clicar  = (el) => {
    if(typeof(el) === "number"){
      if(valor1 === "") {
        setValor1(parseInt(el));
      } else if (valor1 !== "" && operacion === ""){
        let aux = String(valor1)+el;
        setValor1(parseInt(aux));
      } else if (valor2 === ""){
        setValor2(parseInt(el));
      } else if (valor2 !== "" && resultat === ""){
        let aux = String(valor2)+el;
        setValor2(parseInt(aux));
      } else {
        setValor1(parseInt(el));
        setValor2("");
        setResultat("");
        setOperacion("");
      }
    } else if (el === "="){
      setResultat(true);
    } else if (el === "C"){
      setValor1("");
      setValor2("");
      setResultat("");
      setOperacion("");
    } else {
      setOperacion(el);
    }
  }
  return (
    <div className="calculadora">
      <Display num1={valor1} num2={valor2} operacion={operacion} resultat={resultat}></Display>
      <Boton clicar={clicar}> </Boton>
    </div>
  );
}

export default App;
